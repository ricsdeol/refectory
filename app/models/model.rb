# ## Schema Information
#
# Table name: `models`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`id`**            | `integer`          | `not null, primary key`
# **`make_id`**       | `integer`          |
# **`name`**          | `string`           |
# **`created_at`**    | `datetime`         | `not null`
# **`updated_at`**    | `datetime`         | `not null`
# **`webmotors_id`**  | `integer`          |
#
# ### Indexes
#
# * `index_models_on_webmotors_id`:
#     * **`webmotors_id`**
#

class Model < ActiveRecord::Base

  def self.sync_with_webmotors(webmotors_make_id)
    #search the models
    uri = URI("http://www.webmotors.com.br/carro/modelos")

    # Make request for Webmotors site
    make = Make.find_by webmotors_id: webmotors_make_id

    response = Net::HTTP.post_form(uri, { marca: webmotors_make_id })
    models_json = JSON.parse response.body
    # Itera no resultado e grava os modelos que ainda não estão persistidas
    transaction do
      models_json.map do |json|
        Model.where(webmotors_id: json["Id"]).first_or_create do |new_model|
          new_model.name = json["Nome"]
          new_model.make_id = make.id
        end
      end
    end
  end
end
