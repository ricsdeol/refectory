# ## Schema Information
#
# Table name: `makes`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`id`**            | `integer`          | `not null, primary key`
# **`name`**          | `string`           |
# **`webmotors_id`**  | `integer`          |
# **`created_at`**    | `datetime`         | `not null`
# **`updated_at`**    | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_makes_on_webmotors_id`:
#     * **`webmotors_id`**
#

class Make < ActiveRecord::Base

  def self.sync_with_webmotors
    #search the make
    uri = URI("http://www.webmotors.com.br/carro/marcas")
    # Make request for Webmotors site
    response = Net::HTTP.post_form(uri, {})
    json = JSON.parse response.body

    # Itera no resultado e grava as marcas que ainda não estão persistidas
    transaction do
      json.map do |make_params|
        Make.where(webmotors_id: make_params["Id"]).first_or_create do |new_make|
          new_make.name = make_params["Nome"]
        end
      end
    end
  end
end
