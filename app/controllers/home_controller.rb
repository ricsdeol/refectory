class HomeController < ApplicationController
  def index
    @makes = Make.sync_with_webmotors
  end
end
