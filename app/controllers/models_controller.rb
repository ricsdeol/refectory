class ModelsController < ApplicationController
  def index
    @models = Model.sync_with_webmotors params[:webmotors_make_id]
  end
end
