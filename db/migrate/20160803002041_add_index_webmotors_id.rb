class AddIndexWebmotorsId < ActiveRecord::Migration
  def change
    add_index :makes, :webmotors_id
    add_index :models, :webmotors_id
  end
end
